### Provider section
provider "aws" {
  region = var.aws_region
}

# backend section
# Run terraform init --backend-config={PATH/of/backend/config/file}
# and file conent should be 
# key={file-path-in-the-bucket}
# bucket={bucketname}
# terraform {
#   backend "s3" {
#   }
# }

#----------------- Local variables -----------------
locals {
  ami_owner_account_id = "137112412989"
}
#----------------- Data section -----------------
# Get all az in selected region
data "aws_availability_zones" "current" {}

# get latest Amazon Linux AMI in selected region 
data "aws_ami" "latest_amazon_linux" {
  owners      = [local.ami_owner_account_id]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

#----------------- resources section -----------------

module "vpc" {
  source = "git::ssh://git@gitlab.com/gurunathsane/terraform_vpc_module"

  name                 = var.name
  vpc_cidr             = var.vpc_cidr
  enable_dns_hostnames = true
  public_subnet_cidrs  = var.public_subnet_cidrs
  private_subnet_cidrs = var.private_subnet_cidrs
  tags                 = var.tags

}

resource "aws_security_group" "lb_sg" {
  name   = "Security group for LB"
  vpc_id = module.vpc.vpc_id

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge({ "Name" = "${var.name}-lb-sg" }, var.tags)
}


resource "aws_security_group" "webserver_sg" {
  name   = "Web Server Security Group"
  vpc_id = module.vpc.vpc_id

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port       = ingress.value
      to_port         = ingress.value
      protocol        = "tcp"
      security_groups = [aws_security_group.lb_sg.id]
    }
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge({ "Name" = "${var.name}-webserver-sg" }, var.tags)

}

resource "aws_launch_template" "webserver" {
  name                   = "webserver-lt"
  image_id               = data.aws_ami.latest_amazon_linux.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.webserver_sg.id]
  user_data              = templatefile("${path.module}/user_data.sh", { owner = "Gurunath SANE" }) #TODO: check this

  tags = merge({ "Name" = "${var.name}-webserver-lt" }, var.tags)
}


resource "aws_lb_target_group" "webserver" {
  name                 = "webserver-TG"
  vpc_id               = module.vpc.vpc_id
  port                 = 80
  protocol             = "HTTP"
  deregistration_delay = 10 # seconds

  tags = merge({ "Name" = "${var.name}-webserver-tg" }, var.tags)
}

resource "aws_lb" "webserver" {
  name               = "webserver-alb"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = module.vpc.public_subnets

  tags = merge({ "Name" = "${var.name}-webserver-alb" }, var.tags)
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.webserver.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.webserver.arn
  }

  tags = merge({ "Name" = "${var.name}-webserver-listener" }, var.tags)
}

resource "aws_autoscaling_group" "webserver" {
  name                = "web-ha-asg-ver-${aws_launch_template.webserver.latest_version}"
  min_size            = 1
  max_size            = length(data.aws_availability_zones.current)
  min_elb_capacity    = 1
  health_check_type   = "ELB"
  vpc_zone_identifier = module.vpc.private_subnets
  target_group_arns   = [aws_lb_target_group.webserver.arn]

  launch_template {
    id      = aws_launch_template.webserver.id
    version = aws_launch_template.webserver.latest_version
  }

  dynamic "tag" {
    for_each = {
      Name   = "web in ASG-v${aws_launch_template.webserver.latest_version}"
      TAGKEY = "TAGVALUE"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    aws_launch_template.webserver,
    aws_security_group.webserver_sg
  ]
}