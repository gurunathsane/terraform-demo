
variable "aws_region" {
  type    = string
  default = "ap-southeast-1"
}

variable "tags" {
  description = "The default tags attached to every asset created by this module "
  default     = {}
}

variable "name" {
  type        = string
  description = "name prefix for every asset"
}

variable "vpc_cidr" {
  type        = string
  description = "(Optional) The IPv4 CIDR block for the VPC"

  validation {
    condition     = can(cidrhost(var.vpc_cidr, 0))
    error_message = "Must be valid IPv4 CIDR."
  }

  sensitive = false
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks for public subnets"
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks for private subnets"
}

variable "instance_type" {
  type        = string
  default     = "t3.micro"
  description = "Type of AWS instance"
}