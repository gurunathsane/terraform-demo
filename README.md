# Terraform

- [Terraform](#terraform)
- [Introduction](#introduction)
- [Install](#install)
- [Terraform Providers](#terraform-providers)
- [Terraform workflow](#terraform-workflow)
- [Terraform features covered in this repository](#terraform-features-covered-in-this-repository)
  - [AWS Provider](#aws-provider)
  - [Data block](#data-block)
  - [Module](#module)
  - [resources](#resources)
  - [Terraform variables](#terraform-variables)
  - [Terraform output](#terraform-output)
  - [Terraform state file](#terraform-state-file)
  - [Terraform remote state](#terraform-remote-state)
  - [Template file](#template-file)
- [Other Important terraform features](#other-important-terraform-features)
  - [Dynamic blocks](#dynamic-blocks)
  - [Depends on](#depends-on)
  - [resource life cycle](#resource-life-cycle)
  - [local variables](#local-variables)
  - [Conditions](#conditions)
  - [Loops count/for\_each/for in](#loops-countfor_eachfor-in)
  - [Multiple providers in same script](#multiple-providers-in-same-script)
  - [terraform taint and untaint](#terraform-taint-and-untaint)
  - [Forcing Re-creation of Resources](#forcing-re-creation-of-resources)
  - [workspaces](#workspaces)
  - [terraform validate](#terraform-validate)
  - [terraform show command](#terraform-show-command)
  - [terraform refresh](#terraform-refresh)
  - [terraform console](#terraform-console)
  - [Terraform target](#terraform-target)
  - [Terraform fmt](#terraform-fmt)
  - [Terraform graph](#terraform-graph)
  - [Built-in Functions](#built-in-functions)
- [Debugging Terraform](#debugging-terraform)

# Introduction

[Terraform](https://www.terraform.io/) is an __open-source infrastructure as code (IaC)__ tool developed by HashiCorp. It enables users to define and provision infrastructure resources such as virtual machines, databases, load balancers, and many more in a declarative way. Terraform uses a domain-specific language (DSL) called __HCL__ (HashiCorp Configuration Language) to define __infrastructure__ resources __as code__.

One of the main benefits of using Terraform is that it allows __infrastructure to be treated as code__, which means that infrastructure changes can be __versioned__, __reviewed__, and __tested__ just like code changes. This can lead to increased __reliability, consistency, and reproducibility__ of infrastructure deployments.

Other benefits of using Terraform include:

- `Multi-platform support`: Terraform supports many cloud providers and popular platforms, including AWS, Azure, Google Cloud Platform, kubernetes and many more, allowing users to manage infrastructure across multiple cloud providers and tools. Even if your cloud provider or tool is not supported, one can write there own provider and manage that tool with Terraform

- `Modularity`: Terraform allows users to break down infrastructure into __smaller, reusable components__ called __modules__. Modules can be shared and reused across different projects, making it easier to manage infrastructure at scale.

- `Plan and apply workflow`: Terraform provides a __plan and apply__ workflow that allows users to __preview__ infrastructure changes __before they are made__. This helps to prevent unexpected changes and allows for more controlled infrastructure deployments.

Overall, Terraform is a powerful tool for managing infrastructure as code, and can help teams improve their infrastructure management practices and achieve greater __reliability, consistency, and scalability__.

# Install

To use Terraform you will need to install it. HashiCorp distributes Terraform as a __binary package__. You can also install Terraform using popular package managers as listed on [official documentation page](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli). At a high level, you need to find a __binary__ for our workstation distribution and download it. Then we need to __place__ downloaded binary in one of the __PATH directory__ and give it __execute permissions__.

# Terraform Providers

To use the terraform, we first need to choose and configure __provider/s__. There are tons of different providers listed in [official page](https://registry.terraform.io/browse/providers). We can write our own provider for specific cloud or tool. Provider specific documentation tells more about how to configure and use it.

In this repository we used [aws](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) provider. Its [documentation page](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) has enough information on how to configure aws access keys and how to use it.  

# Terraform workflow
The core Terraform workflow consists of three stages:

- `Write:` You define resources, which may be across multiple cloud providers and services. For example, you might create a configuration to deploy an application on virtual machines in a Virtual Private Cloud (VPC) network with security groups and a load balancer.
- `Plan:` Terraform creates an execution plan describing the infrastructure it will create, update, or destroy based on the existing infrastructure and your configuration.
- `Apply:` On approval, Terraform performs the proposed operations in the correct order, respecting any resource dependencies. For example, if you update the properties of a VPC and change the number of virtual machines in that VPC, Terraform will recreate the VPC before scaling the virtual machines.

    ![terraform flow](./readme-assets/terraform-flow.png)

# Terraform features covered in this repository 

## AWS Provider 
The __AWS provider__ is one of the many providers that Terraform supports, and it enables you to provision and manage resources on the __Amazon Web Services__ (AWS) cloud platform.

The AWS provider supports a wide range of AWS services, including EC2, S3, RDS, IAM, and many more. You can find the full list of supported services and resources in the [Terraform AWS provider documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs).

To use the AWS provider in Terraform, you need to configure it with your AWS access and secret keys. You can then define your infrastructure resources using Terraform's declarative language, HCL (HashiCorp Configuration Language), and specify the desired state of the resources. When you run terraform apply, Terraform will create or update the resources to match the desired state.

In [main.tf](main.tf) file we have a section 

```
provider "aws" {
    region = var.aws_region
}
```

Where region is picked up from variable. Access and secret keys are picked up from aws config file (~/.aws/credentials) or environment variables. When we run terraform init command, terraform downloads said provider from internet and stores it in `.terraform/providers` directory.

## Data block
In Terraform, a __data block__ is used to retrieve and access data that is already present in your infrastructure or from external sources, such as APIs or other services. It allows you to query and extract specific information about your infrastructure, and then use that data elsewhere in your Terraform configuration.

The syntax for a data block is similar to a resource block, but instead of creating a new resource, it defines a data source that Terraform can query. In [main.tf](main.tf) we have data block example which retrieves information about an AWS AMI (Amazon Machine Image) in configured region:

```
data "aws_ami" "latest_amazon_linux" {
    owners      = ["137112412989"]
    most_recent = true
    filter {
        name   = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
}
```

here, the aws_ami data source queries AWS to find the most recent Amazon linux AMI that meets the specified filters. The resulting data can then be used elsewhere in the Terraform configuration, such as in a resource block that creates an EC2 instance.

Using data blocks allows you to separate data retrieval from resource creation, which can make your Terraform configuration more modular and easier to manage. It also allows you to reuse the same data across multiple resources, avoiding the need to duplicate the same query logic in multiple places.

## Module
In Terraform, a __module__ is a reusable unit of Terraform configuration that __encapsulates a set of resources and their dependencies.__ It allows you to package and distribute your infrastructure code as a reusable component, which can be easily shared and reused across different Terraform configurations.

A module can contain one or more resource blocks, data blocks, variables, and outputs. It can also define its own input variables, which can be used to parameterize the module and make it more flexible.

There are thousands of modules already available on terraform registry which managed and maintained by official providers or open source community. One can write there own modules as per business requirement and host it `publicly` on terraform registry or `privately` on any git repository. Just for example/experiment I written a [terraform_vpc_module](https://gitlab.com/gurunathsane/terraform_vpc_module) which creates networking components (VPC, Private and public subnets, route tables, Nat gateway, Internet gateway). This module is publicly available. in this example I used this module.

In main.tf, we used the terraform_vpc_module 

```
module "vpc" {
    source = "git::ssh://git@gitlab.com/gurunathsane/terraform_vpc_module"

    name                 = var.name
    vpc_cidr             = var.vpc_cidr
    enable_dns_hostnames = true
    public_subnet_cidrs  = var.public_subnet_cidrs
    private_subnet_cidrs = var.private_subnet_cidrs
    tags                 = var.tags
}
```
Here, first line is source. The source argument can be a local or remote file/s path, a Git URL, or a terraform registry URL. Here we given git URL for our module. When we init terraform project, it downloads modules code in case of remote modules (eg. git repo/terraform registry link) and stores and use the module in `.terraform/modules` directory.

rest arguments are module specific. some of them are required and some of them are optional with default values. Terraform modules also export some values which we can use in resources block. More information on arguments and outputs variables can be found in documentation of the module. In this example our module created vpc and exports vpc_id as a output and we can use this values anywhere in the terraform code with reference module.vpc.vpc_id.

## resources

In Terraform, a __resource__ is a declarative representation of an infrastructure object that you want to create, update, or delete. It defines a __set of attributes and properties__ that describe the characteristics of the object you are creating, such as its type, name, configuration settings, and dependencies.

In [main.tf](main.tf) we created multiple resources. Simple example can be:
```
resource "aws_lb" "webserver" {
    name               = "webserver-alb"
    load_balancer_type = "application"
    security_groups    = [aws_security_group.lb_sg.id]
    subnets            = module.vpc.public_subnets

    tags = merge({ "Name" = "${var.name}-webserver-alb" }, var.tags)
}
```
This Terraform block creates an AWS load balancer called webserver, by defining an aws_lb resource. The block sets several attributes to configure the load balancer, including its name, type, security group, subnets, and tags.

The name and type of the load balancer are set as string values using the name and type attributes of the resource block, respectively. The security group lb_sg is defined using another resource block that creates an aws_security_group and its id referred in list for security_groups attribute of load balancer. Similarly, subnets are created in vpc module and referred its ids in load balancer subnets attribute. The block sets tags for the load balancer by modifying a variable named tags. The variable is defined as variable in the configuration, and contains a map of key-value pairs that are used as tags for the load balancer.

## Terraform variables
Terraform __variables__ are used to represent values that can be passed to Terraform configuration files, such as `.tfvars` files, at runtime. They allow you to define reusable values that can be used across multiple resources, modules, or environments. More information on variables can be found at [official documentation](https://developer.hashicorp.com/terraform/language/values/variables)

We can define variable in any .tf file. Generally we use dedicated file  called variables.tf to define variables in terraform. In this repository we defined many terraform variables in [variables.tf](./variables.tf) file. for example:
```
variable "vpc_cidr" {
    type        = string
    description = "(Optional) The IPv4 CIDR block for the VPC"

    validation {
        condition     = can(cidrhost(var.vpc_cidr, 0))
        error_message = "Must be valid IPv4 CIDR."
    }

    sensitive = false
}
```
here we are defining variable called vpc_cidr. We can use its value anywhere in the terraform script like this: `var.vpc_cidr`

In Terraform, variables have different attributes that define their behavior and usage within the configuration. Some of the common attributes of variables in Terraform are:

- `type:` This attribute specifies the data type of the variable, such as string, number, list, map, etc. Terraform uses this information to validate the value passed to the variable at runtime. in example above vpc_cidr is string type variable. 
- `default:` This attribute defines a default value for the variable in case it is not set explicitly during runtime. If a default value is provided, the variable becomes optional, and Terraform will not prompt for its value during execution.
- `description:` This attribute provides a description of the variable, which can be useful for documenting its purpose and usage.
- `validation:` This attribute allows you to define custom validation rules for the variable, such as enforcing a specific format or range for the value. Terraform will validate the value against these rules during runtime.
- `sensitive:` This attribute marks the variable as sensitive, which means its value will be redacted in the logs and output of Terraform. This is useful for variables that contain sensitive data, such as passwords or API keys.

    In Terraform, you can set variable values in several ways. Here are all possible methods:
- `Command-line flags:` You can pass variable values to Terraform using -var or -var-file command-line flags. For example:

    ```
    terraform apply -var 'region=us-east-1'
    ```
    This will set the value of the region variable to us-east-1

- `Environment variables:` You can also set variable values using environment variables that start with TF_VAR_. For example:
    ```
    export TF_VAR_region=us-east-1
    ```
    This will set the value of the region variable to us-east-1 while running terraform apply

- `values file`: if there are many variables to pass from command line, we can create a variables file with extension .tfvars and pass its path for terraform commands. example variables file content is:
    ```
    name = "my-infra"
    public_subnet_cidrs  = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    private_subnet_cidrs = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
    ```
    
    Here we are defining values for 3 variables. Name is string type and its value is my-infra, public and private subnet cidrs are list of strings. We can pass values file to terraform command like this:
    ```
    terraform apply -var-file='variables.tfvars'
    ```

- `Terraform configuration files:` You can define default values for variables in your Terraform configuration files using the default attribute. For example:
    ```
    variable "region" {
        type    = string
        default = "us-west-2"
    }
    ```
    This will set the default value of the region variable to us-west-2. If you do not set the value of the variable using one of the other methods, Terraform will use this default value.

- `Interactive prompts:` If a variable is not set using any of the above methods, Terraform will prompt you for its value during execution.

It's important to note that variables set using command-line flags or environment variables take precedence over default values defined in the configuration files. Also, sensitive variables, such as passwords or API keys, should not be set using environment variables or command-line flags since they can be visible to other users on the same system. Instead, it's recommended to use a secure mechanism to manage sensitive variables, such as using a secrets management tool.

## Terraform output
The __output__ block is used to define values that should be made available outside of the Terraform configuration, such as information about resources created by the configuration.

__Outputs__ are useful for __passing information__ between Terraform configurations or to other tools outside of Terraform, such as monitoring tools or CI/CD pipelines.

We can define output block in any .tf file, but as a standard practice, we keep them in separate file with name `outputs.tf`. We defined one output in [outputs.tf](./outputs.tf) file
```
output "web_loadbalancer_url" {
    value = aws_lb.webserver.dns_name
}
```

In this example, we define an output named web_loadbalancer_url, which is URL of the load balancer created by this example. This output can be used in other Terraform configurations or passed to other tools as needed.

To view the outputs of a Terraform configuration, you can use the terraform output command `terraform output web_loadbalancer_url`.
This will display the value of the web_loadbalancer_url output in the console.

You can also use the -json flag to output the values in JSON format, which can be useful for programmatic use. For example: `terraform output -json web_loadbalancer_url`

This will output the value of the web_loadbalancer_url output in JSON format, which can be easily parsed by other tools.

Note that outputs are only available after a configuration has been applied, as they depend on the creation and provisioning of resources. Therefore, you need to run terraform apply at least once before using terraform output.

## Terraform state file
In Terraform, the state refers to the __information about the resources__ created by a configuration, including their current state and metadata. Terraform uses this information to manage resources and ensure that they remain in the desired state.

The state is stored in a file named `terraform.tfstate`, which is created and managed by Terraform. This file can be stored locally on your machine or remotely in a shared location, such as an S3 bucket or a Consul cluster.

When you run `terraform apply/plan`, Terraform reads the configuration and uses it to create or update resources. As it does so, it updates the state file to reflect the current state of the resources. This includes information such as resource IDs, IP addresses, and other metadata that may be needed for future operations.

The state file is also used to __track dependencies between resources__ and to ensure that changes are applied in the correct __order__. For example, if you modify a security group that is attached to an EC2 instance, Terraform will use the state file to determine that the security group needs to be updated before the instance.

To view the current state of your resources, you can use the `terraform state list` command to list the resources in the state file, and the `terraform state show` command to display detailed information about a specific resource. For example:
```
terraform state list
terraform state show aws_security_group.lb_sg
```

It's important to note that the state file contains __sensitive__ information, such as resource IDs and credentials, and should be treated as a secret. You should never share the state file with anyone who should not have access to your infrastructure. To prevent accidental deletion or modification of the state file, you can also enable state locking, which prevents multiple users from modifying the state file at the same time.

## Terraform remote state
In Terraform, you can __store__ your __state file__ in a __remote location__, such as an S3 bucket, to enable collaboration between team members and to provide a more reliable and scalable way of storing your state. All possible backend storage listed [here](https://developer.hashicorp.com/terraform/language/settings/backends/configuration). We can use one or more backends to have more availability

in this example we have a terraform backend configuration block in [main.tf](main.tf) file
```
terraform {
    backend "s3" {
    }
}
```
While initializing terraform with `terraform init` command, we need to pass flag `--backend-config` the value should be path to backend config files. In case of s3 backend, this file content should be 
```
key={file-path-in-the-bucket}
bucket={bucket-name}
```
we can also pass these 2 parameters in backend "s3" section as well but this is not good practice. 

while run `terraform init --backend-config={path-of-file}` to initialize Terraform with the new backend configuration. This will configure Terraform to use the S3 backend to store the state file. After this you can use Terraform as you normally would, using terraform plan and terraform apply to create and modify resources. Terraform will automatically store the state file in the S3 bucket you configured.

## Template file
Terraform's __templatefile function__ allows you to __dynamically__ generate __configuration files__ by using a template and replacing placeholders with actual values. This function can be useful for creating __reusable configurations__ or generating configuration files __that depend on runtime values__.

In [main.tf](./main.tf) We define an EC2 launch template resource that has a user_data field that is generated by calling the templatefile function with the path to the template file and the map of variables. 

In template [user_data.sh](./user_data.sh), we use the variables passed in to generate a simple bash script that outputs the instance information and owner information.
 When Terraform runs, it will replace the placeholders in the template with the actual values of the variables, generating a custom script for each instance.

# Other Important terraform features
## Dynamic blocks
In Terraform, a __dynamic block__ is a __way to generate repeatable blocks of configuration__ based on a __dynamic list__ of values. This allows you to create more __flexible__ and __reusable__ Terraform code that can handle varying numbers of resources.

__Dynamic blocks__ are typically used in situations where you need to create a variable number of similar resources, such as multiple subnets, security groups, or IAM roles. Using a dynamic block, you can specify a list of values for a given resource, and Terraform will generate a block of configuration for each value in the list.

In [main.tf](./main.tf) file, we used dynamic blogs in aws_security_group resource.
```
dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
```
In this example, it will iterate through list ["80", "443"] and create 2 ingress rules. In first iteration ingress.value is 80 and in next iteration it will be 443

## Depends on
The __depends_on__ parameter is used to __define explicit dependencies between resources__. When a resource has a depends_on parameter, it will be created or updated only after the resources it depends on have been created or updated.

The depends_on parameter is useful when you have resources that have __implicit dependencies__ that Terraform is not able to determine on its own. For example, if you have a security group that needs to be created before you can launch an EC2 instance, you can use depends_on to explicitly specify the dependency. 

In [main.tf](./main.tf) for resource aws_autoscaling_group - webserver there is depends_on block 
```
depends_on = [
    aws_launch_template.webserver,
    aws_security_group.webserver_sg
]
```
Here the aws_autoscaling_group resource depends on the aws_security_group and aws_launch_template resource, so Terraform will create or update the security group and launch template before creating or updating the auto scaling group.

## resource life cycle
The __lifecycle__ block is used to configure certain aspects of the resource lifecycle for a particular resource. The lifecycle block is typically used to modify the default behavior of Terraform for specific use cases.

Here are some common use cases for the lifecycle block in Terraform:
- `Prevent deletion of a resource:` You can use the prevent_destroy parameter to prevent a resource from being deleted. This is useful for resources that you want to ensure are never deleted accidentally, such as a production database.
    ```
    resource "aws_instance" "example" {
        # ...
        lifecycle {
            prevent_destroy = true
        }
    }
    ```

- `Ignore changes to specific attributes:` You can use the ignore_changes parameter to ignore changes to specific attributes of a resource. This is useful for attributes that may change frequently but don't affect the resource's functionality, such as a resource's tags.
    ```
    resource "aws_instance" "example" {
        # ...
        lifecycle {
            ignore_changes = [
                tags,
            ]
        }
    }
    ```

- `Configure create before destroy behavior:` You can use the create_before_destroy parameter to configure how Terraform should handle resource updates. This parameter tells Terraform to create the new resource before destroying the old one, which can be useful for preventing downtime during updates.
    ```
    resource "aws_instance" "example" {
        # ...
        lifecycle {
            create_before_destroy = true
        }
    }
    ```

## local variables
__Local variables__ are used to __define values__ that are used within a module or configuration file but are __not exposed__ as inputs or outputs. These variables are defined using the `locals block` and are scoped to the module or configuration file where they are defined.

In [main.tf](./main.tf) file we have a local section as:
```
locals {
  ami_owner_account_id = "137112412989"
}
```
We can then use this variable in other parts of our configuration file for example we are using local.ami_owner_account_id in data - aws_ami block
```
data "aws_ami" "latest_amazon_linux" {
  owners      = [local.ami_owner_account_id]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
```

## Conditions
In Terraform, you can use __conditions__ to __control__ when resources or modules are created or modified __based on certain conditions__ or variables.

The basic syntax for using conditions in Terraform is:
```
resource "aws_instance" "example" {
  count = var.create_instance ? 1 : 0
  
  # ... other configuration ...
}
```
In this example, the `count` parameter is used to conditionally create an EC2 instance based on the value of the create_instance variable. If the variable is true, one instance will be created; if the variable is false, no instances will be created. In some programming languages it is called as ternary operator. 

Another way to use conditions in Terraform is with the if and _for_each_ parameters, which can be used to __conditionally__ include or exclude resources or modules __based on a particular condition__ or set of conditions.
```
resource "aws_instance" "example" {
  # ... other configuration ...

  lifecycle {
    ignore_changes = [
      # ignore changes to tags if the instance is stopped
      tags
    ]
  }

  # only add a tag if the instance is running
  dynamic "tags" {
    for_each = aws_instance.example.state == "running" ? [1] : []
    content {
      key   = "Name"
      value = "MyInstance"
    }
  }
}
```
In this example, the dynamic block is used to conditionally add a tag to the instance only if its state is "running". If the instance is not running, the for_each parameter will evaluate to an empty list, and the tag will not be added.

## Loops count/for_each/for in 
You can use __loops__ to create multiple instances of a resource or module based on a list, set, or map of values.

The basic syntax for using loops in Terraform is:
```
resource "aws_instance" "example" {
  count = 2

  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"

  tags = {
    Name = "example-${count.index}"
  }
}

```
In this example, the `count` parameter is used to create two EC2 instances with different Name tags based on the value of `count.index`.

Another way to use __loops__ in Terraform is with the `for_each` parameter, which allows you to create multiple instances of a resource or module based on a map of values:
```
locals {
  instances = {
    example1 = { ami = "ami-0c55b159cbfafe1f0", instance_type = "t2.micro" },
    example2 = { ami = "ami-0c55b159cbfafe1f0", instance_type = "t2.nano" },
    example3 = { ami = "ami-0c55b159cbfafe1f0", instance_type = "t2.small" },
  }
}

resource "aws_instance" "example" {
  for_each = local.instances

  ami           = each.value.ami
  instance_type = each.value.instance_type

  tags = {
    Name = each.key
  }
}
```
In this example, the `for_each` parameter is used to create three EC2 instances with different instance types based on the instances map defined in the locals block.

Similar to `for_each`, you can use the for expression to loop over a list, set, or map of values and perform an action on each value. The `for` expression can be used to create multiple instances of a resource or module, or to generate a list or map of values that can be used elsewhere in your configuration.

The basic syntax for using the `for` expression in Terraform is:
```
locals {
  my_list = ["one", "two", "three"]
}

output "my_output" {
  value = [
    for val in local.my_list :
    val
  ]
}
```
In this example, the `for` expression is used to generate a list of values based on the my_list local variable. The resulting list will contain the same values as my_list.

Another way to use the `for` expression in Terraform is to create multiple instances of a resource or module:
```
locals {
  instance_types = ["t2.micro", "t2.nano", "t2.small"]
}

resource "aws_instance" "example" {
  count = length(local.instance_types)

  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = local.instance_types[count.index]

  tags = {
    Name = "example-${count.index}"
  }
}
```
In this example, the `for` expression is used to loop over the instance_types list defined in the locals block. The count parameter is set to the length of the instance_types list, which will create one instance of the aws_instance resource for each value in the list.

## Multiple providers in same script
You can use __multiple providers__ to manage resources in different cloud platforms or environments. This can be useful if you have a __hybrid infrastructure__ that spans multiple cloud providers, __or__ if you need to manage resources in __multiple environments__ eg. production and development environments. 
To use multiple providers in Terraform, you first need to define each provider in your configuration using the `provider block`. For example:
```
provider "aws" {
  region = "us-west-2"
}

provider "azure" {
  features {}
}
```
In this example, we have defined two `providers` aws and azure. The aws provider is configured to use the us-west-2 region, while the azure provider has no additional configuration options. Once you have defined your `providers`, you can use them to manage resources in your configuration by specifying the provider in the resource block. For example:
```
resource "aws_instance" "example" {
  provider = aws
  
  ... other configs
}

resource "azure_virtual_machine" "example" {
  provider = azure

  .. other configs
}
```
In this example, we are using the `aws provider` to create an EC2 instance and the `azure provider` to create an Azure virtual machine. Each resource block specifies the `provider` to use using the `provider parameter`.

We can __even__ use __same type of provider multiple times__ in same configuration files. For example: you can use multiple `aws providers` to manage resources in __different regions, accounts or profiles__. This can be useful if you have resources in multiple regions or accounts, or if you need to manage resources with different permissions or configurations.

To use multiple `aws providers` in Terraform, you first need to define each `provider` in your configuration using the `provider block`, and give them distinct names. For example:
```
provider "aws" {
  region = "ap-south-1"
}

provider "aws" {
  alias  = "secondary"
  region = "us-east-1"
  profile = "secondary-profile"
}
```
In this example, we have defined `two aws providers` : aws and aws.secondary. The first provider is configured to use the ap-south-1 region and has no additional options, while the second provider is configured to use the us-east-1 region and has a different profile set using the profile parameter.
Once you have defined your providers, you can use them to manage resources in your configuration by specifying the provider in the resource block using the provider parameter and the provider alias. In our example for first provider we are not defining any alias, so it become a default aws provider. If we not mention provider in resource section then terraform use the default provider.

```
resource "aws_instance" "example" {
  provider = aws

  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"
  tags = {
    Name = "example"
  }
}

resource "aws_s3_bucket" "example" {
  provider = aws.secondary

  bucket = "example-bucket"
  acl    = "private"
}
```
in this example, we are using the `aws provider` to create an EC2 instance in the ap-south-1 region and the `aws.secondary` provider to create an S3 bucket in the us-east-1 region with a different profile. Each resource block specifies the provider to use using the provider parameter and the provider alias.

## terraform taint and untaint
__terraform taint__ is a command that allows you to mark a resource as `tainted`, which means that Terraform considers it to be in an __unknown or potentially corrupted state__. When a resource is __tainted__, Terraform will __destroy__ it and __recreate__ it on the next `terraform apply` command.

Here's an example of how to use terraform taint:
```
terraform taint aws_instance.my_instance
```

In this example, we are marking an aws_instance resource called my_instance as __tainted__. This resource __will be destroyed and recreated__ on the next `terraform apply` command.

You can use `terraform untaint` command to __remove the tainted state__ from a resource, for example:
```
terraform untaint aws_instance.my_instance
```
_It's important to use terraform taint with caution, as it can cause unexpected changes to your infrastructure. Tainting a resource can also cause Terraform to destroy and recreate other resources that depend on it, which can lead to downtime or other issues. Therefore, it's recommended to use this command only when necessary and with a clear understanding of the potential consequences._

## Forcing Re-creation of Resources
During planning, by default Terraform __retrieves__ the __latest state__ of each existing object and compares it with the current configuration, planning actions only against objects whose current state does not match the configuration.

However, in some cases a __remote object may become damaged or degraded__ in a way that Terraform __cannot__ automatically __detect__. For example, if software running inside a virtual machine crashes but the virtual machine itself is still running then Terraform will typically have no way to detect and respond to the problem, because Terraform only directly manages the machine as a whole.

If you know that an object is __damaged__, or if you want to __force__ Terraform to __replace__ it for any other reason, you can override Terraform's default behavior using the `-replace` option when you run `terraform apply` as shown in the example.
```
terraform apply -replace=aws_instance.my_instance
```

## workspaces
__Workspaces__ are a __way to manage multiple instances__ of the same infrastructure __within a single Terraform configuration__. Workspaces allow you to create multiple instances of your infrastructure, each with its own state file, but all managed by the __same configuration__ code.

Using __workspaces__, you can __maintain multiple environments__, such as development, staging, and production, within a __single configuration__. Each workspace has its own state file, allowing you to manage the __state__ of each environment __independently__. This can be useful for testing changes in a development environment before promoting them to production.

To create a new workspace, you can use the terraform workspace new command:
```
terraform workspace new dev
```
This command will create a __new workspace__ named `dev`. You can then use the `terraform workspace select` command to switch to the new workspace:
```
terraform workspace select dev
```
This command will select the `dev` workspace, allowing you to manage the infrastructure for that environment __independently__.

You can list all of the available workspaces using the `terraform workspace list` command This command will __list all__ of the available __workspaces__, allowing you to see which workspaces are currently available.

When you run `terraform apply` or `terraform plan`, Terraform will __automatically use the state file__ for the __selected workspace__. This allows you to manage the state of each environment __independently__, without interfering with the state of other environments.

## terraform validate
The `terraform validate` is a command in Terraform that __checks the syntax and structure__ of a Terraform configuration file/s. The terraform validate command is used to validate the syntax of all `.tf` files in the current directory or a specified directory.

When you run `terraform validate`, Terraform checks the syntax and structure of the configuration files to ensure that they are valid. The command returns a message indicating whether the configuration files are valid or whether there are any errors.

For example, if you run terraform validate and your configuration files contain syntax errors or invalid constructs, Terraform will report the errors and give you an error message indicating where the errors occurred.

## terraform show command
The `terraform show` command is used to `provide human-readable output` from a state or plan file. This can be used to inspect a plan to ensure that the planned operations are expected, or to inspect the current state as Terraform sees it.

If you run `terraform show` in a directory containing a Terraform configuration, it will display the current state of your infrastructure in __human-readable__ format. This includes information about the resources that have been created, their attributes, and any dependencies between them.

If you run `terraform show` with an argument pointing to a Terraform plan file (which is produced by running `terraform plan`), it will display the proposed changes to your infrastructure in a __human-readable__ format. This includes the resources that will be created, modified, or destroyed, and any dependencies between them.
Machine-readable output is generated by adding the `-json` command-line flag.

## terraform refresh 
The `terraform refresh` command is used to __reconcile__ the current __state of__ your __infrastructure__ with the state that Terraform has recorded in its state file. This command compares the current state of your infrastructure with the desired state defined in your Terraform configuration and updates the state file with any changes it finds.

The `terraform refresh` command performs a __read-only__ operation on your infrastructure, which means __it does not make__ any __changes to the resources__ themselves. Instead, it __only updates the state file__ with any __changes__ that have __occurred outside of Terraform's__ management. This can be useful if you have made changes to your infrastructure outside of Terraform, such as manually updating a resource, and need to bring your state file up-to-date.

To use the `terraform refresh` command, you simply navigate to the directory where your Terraform configuration is stored and run the command `terraform refresh`

When you run this command, Terraform will connect to your infrastructure provider and query the current state of your resources. It will then compare this with the state recorded in the Terraform state file and update the state file with any changes it finds.

_It's important to note that the `terraform refresh` command should not be used as a replacement for running `terraform apply` to make changes to your infrastructure. Instead, it should be used as a __tool__ to __ensure__ that your __state file is up-to-date__ before making changes to your infrastructure with `terraform apply`._

## terraform console
the `terraform console` command allows you to open a console session where you can interactively query and manipulate the values of your Terraform configuration.

The `terraform console` command provides an interactive command-line environment where you can enter expressions and see their resulting values, including any interpolations and functions used in your configuration. This can be useful for testing out expressions, troubleshooting issues, or simply exploring the values in your configuration.

To use the `terraform console` command, you simply navigate to the directory where your Terraform configuration is stored and run the command `terraform console`

When you run this command, Terraform will open an interactive console where you can enter expressions and see their resulting values. For example, you can enter expressions like `aws_region`

And Terraform will output the value of the `aws_region` variable. You can also use the console to __test__ out more __complex expressions__, such as interpolations or functions. For example, you can enter an interpolation like:
```
"${var.instance_type}-web-${count.index}"
```
And Terraform will output the resulting value of the interpolation.

We can use terraform console to test complex conditions, terraform functions and many more. 

## Terraform target 
The `terraform target` is a command in Terraform that __allows__ you to specify a __specific resource__ or set of resources that you __want to apply changes to or destroy__. The terraform target command is __used to limit the scope__ of the `terraform apply` or `terraform destroy` commands to a __specific__ set of __resources__.

When you run terraform target, you can specify the address of the resource or resources that you want to target. The address is a string that identifies the resource within the Terraform configuration. 

Here's an example of how to use terraform target to limit the scope of the terraform apply command:
```
terraform target aws_instance.my_instance aws_s3_bucket.my_bucket
```
This will target the aws_instance.my_instance and aws_s3_bucket.my_bucket resources, meaning that when you run `terraform apply`, only changes to that resource will be applied. State of the rest of resources will not checked at all. 

It's important to note that using terraform target does not affect the Terraform state, so if you target a subset of resources and then run terraform plan or terraform apply, Terraform will still consider the entire state when calculating changes.

## Terraform fmt
`terraform fmt` is a command in Terraform that automatically updates the formatting of your Terraform configuration files. The `terraform fmt` command is used to format all .tf files in the current directory or a specified directory.

When you run `terraform fmt`, Terraform will update your configuration files to follow the recommended Terraform style conventions, such as indentation, line wrapping, and commenting. This command will modify your files in place, so it's recommended to make a backup of your files before running this command.

The `terraform fmt` command is a helpful tool to ensure that your Terraform code is consistent and readable. It helps to avoid formatting issues that could cause confusion and makes it easier to collaborate with other team members.

It's a good practice to run `terraform fmt` before committing any changes to your version control system to ensure that your code follows the recommended formatting conventions.

## Terraform graph
The `terraform graph` command is used to generate a visual representation of the dependency graph of your infrastructure resources. The dependency graph shows the relationship between resources, including which resources depend on others, and which resources are being created or destroyed.

The `terraform graph` command generates the dependency graph in the `Graphviz DOT format`, which can be visualized using a Graphviz viewer. [Graphviz](https://graphviz.org/) is an open-source tool that can be used to create visual representations of graphs and networks.

To use the `terraform graph` command, you simply navigate to the directory where your Terraform configuration is stored and run the command:
```
terraform graph
```
When you run this command, Terraform will generate a `DOT-formatted` representation of the dependency graph for your infrastructure. You can then use a `Graphviz` viewer to visualize the graph.

For example, you can use the dot command from Graphviz to generate an image of the graph:
```
terraform graph | dot -Tpng > graph.png
```
This command will generate a PNG image of the graph and save it to a file named graph.png.

The dependency graph generated by the terraform graph command can be useful for understanding the relationships between resources in your infrastructure and identifying any circular dependencies or other issues. It can also be useful for communicating the structure of your infrastructure to other members of your team or stakeholders.

for this example this output of `terraform graph` command is stored in [graph.dot](./readme-assets/graph.dot) file. When we convert this to .png file it looks like this
![dot graph](./readme-assets/graph.png)

## Built-in Functions
Terraform includes many built-in functions that can be used in your configuration files to manipulate values, perform calculations, and more. These functions can be used in a variety of contexts, including resource definitions, variable definitions, and output definitions. [This official document](https://developer.hashicorp.com/terraform/language/functions) explains and lists all available functions.

Some examples of built-in functions in Terraform include:
- `min` and `max`: These functions return the minimum or maximum value of a list of numbers. For example:
    ```
    max(1, 2, 3)  # returns 3
    min(1, 2, 3)  # returns 1
    ```
- `concat`: This function concatenates a list of strings into a single string. For example:
    ```
    concat(["Hello", " ", "world"])  # returns "Hello world"
    ```
- `element`: This function returns the element of a list at a specified index. For example:
    ```
    element(["a", "b", "c"], 1)  # returns "b"
    ```
- `format` This function formats a string using placeholders for values. For example:
    ```
    format("Hello %s", "world")  # returns "Hello world"
    ```
- `length`: This function returns the length of a string or list. For example:
    ```
    length("Hello world")  # returns 11
    length(["a", "b", "c"])  # returns 3
    ```

there are hundreds of functions available. We used couple of them in this repository for example:
- `merge`: it takes an arbitrary number of maps or objects, and returns a single map or object that contains a merged set of elements from all arguments.
    ```
    merge({a="b"}, {a=[1,2], c="z"}, {d=3})
    ```
    Will return 
    ```
    {
        "a" = [
            1,
            2,
        ]
        "c" = "z"
        "d" = 3
    }
    ```
- `templatefile`: it reads the file at the given path and renders its content as a template using a supplied set of template variables.
- `can`: evaluates the given expression and returns a boolean value indicating whether the expression produced a result without any errors.
- `cidrhost`: calculates a full host IP address for a given host number within a given IP network address prefix.

# Debugging Terraform
Terraform has detailed logs that you can enable by setting the `TF_LOG` environment variable to any value. Enabling this setting causes detailed logs to appear on stderr.

You can set `TF_LOG` to one of the log levels (in order of decreasing verbosity) TRACE, DEBUG, INFO, WARN or ERROR to change the verbosity of the logs.

Setting `TF_LOG` to JSON outputs logs at the TRACE level or higher, and uses a parsable JSON encoding as the formatting.

Logging can be enabled separately for terraform itself and the provider plugins using the `TF_LOG_CORE` or `TF_LOG_PROVIDER` environment variables. These take the same level arguments as `TF_LOG`, but only activate a subset of the logs.

To persist logged output you can set `TF_LOG_PATH` in order to force the log to always be appended to a specific file when logging is enabled. Note that even when `TF_LOG_PATH` is set, `TF_LOG` must be set in order for any logging to be enabled.
