#-------------------------------------------------------------------------------
output "web_loadbalancer_url" {
  value = aws_lb.webserver.dns_name
}